\documentclass[a4paper,10pt]{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[hidelinks]{hyperref}
\usepackage{graphicx}
\usepackage{subfig}

\usepackage{minted}
\usepackage{xcolor}
\usepackage{color, colortbl}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[textwidth=16cm]{geometry}
\begin{document}

\setlength{\parindent}{0em}
\setlength{\parskip}{1em}

\title{Exploring Convolution and Attention \\applied to Audio Classification}
\author{Anton Blomström}
\date{\today}
\maketitle

\begin{abstract}
  There has been a push to explore the transformer architecture and
  self-attention mechanisms overall recently. This is due to its ability
  to model longer sequences and be trained more efficiently than
  recurrent neural networks (RNNs), it has seen adoption in both
  language modeling (such as GPT), and audio speech recognition (ASR).
  In this report, we explore and confirm some of the results shown
  in these research papers.
\end{abstract}
\thispagestyle{empty}

\newpage

\section{Introduction}
Audio classification has its applications in collection of metrics
that would otherwise be difficult to gather, such as gunshots in cities,
and preservation efforts of endangered animals. The latter of these is
what this report uses to explore. Namely, the Rainforest Connection (RFCX)
species audio detection dataset\cite{dataset}.

We compare hyper-parameter searches of three naive neural networks,
comprised of a convolutional neural network (CNN)\cite{CNN}, a CNN with
multi-head attention (CNN-MHA) layers after it, and a neural network with
only multi-head attention (MHA). These are then compared to ResNet50\cite{resnet},
and Conformer\cite{conformer}.

\subsection{Previous work}
In the past, RNNs such as Long-Short Term Memory (LSTM)\cite{LSTM},
Gated Recurrent Unit (GRU)\cite{GRU}, and CNNs have been used to
classify audio\cite{LSTM:audio}\cite{GRU:audio}\cite{CNN:audio}.

\section{Problem specification}

The Kaggle competition is described in the following ways:

\begin{quote}
  ``In this competition, you’ll automate the detection of bird and frog
  species in tropical soundscape recordings. You'll create your models with
  limited, acoustically complex training data. Rich in more than bird and frog
  noises, expect to hear an insect or two, which your model will need to filter out.''

  \dots

  ``The task consists of predicting the species present in each test audio file.
  Some test audio files contain a single species while others contain multiple.
  The predictions are to be done at the audio file level, i.e., no start/end
  timestamps are required. ''
\end{quote}

More specifically, the objective is to classify one minute long audio files into a
multi-label classification of length 24. The metric used to score submissions is label-
weighted label-ranking average precision (lwlrap).

\newpage

\subsection{LRAP metric}

Given a binary indicator matrix of ground-truth labels
\begin{equation}
  y \in \{0, 1\}^{n_\text{samples} \times n_\text{labels}}
\end{equation}
The score associated with each label is denoted by $\hat{f}$ where,
\begin{equation}
  \hat{f} \in \mathbb{R}^{n_\text{samples} \times n_\text{labels}}
\end{equation}

Then we can calculate LRAP using the following formula:
\begin{equation}
  LRAP(y, \hat{f}) = \frac{1}{n_{\text{samples}}}
  \sum_{i=0}^{n_{\text{samples}} - 1} \frac{1}{||y_i||_0}
  \sum_{j:y_{ij} = 1} \frac{|\mathcal{L}_{ij}|}{\text{rank}_{ij}}
\end{equation}
where
\begin{equation}
  \mathcal{L}_{ij} = \left\{k: y_{ik} = 1, \hat{f}_{ik} \geq \hat{f}_{ij} \right\}
\end{equation}
and
\begin{equation}
  \text{rank}_{ij} = \left|\left\{k: \hat{f}_{ik} \geq \hat{f}_{ij} \right\}\right|
\end{equation}
\begin{center}
  \scriptsize{\href{https://scikit-learn.org/stable/modules/model_evaluation.html\#label-ranking-average-precision}{https://scikit-learn.org/stable/modules/model\_evaluation.html\#label-ranking-average-precision}}
\end{center}

Multiplying with the inverse class frequency gives us the balanced lrap version, lwlrap.

The lwlrap score is then averaged over the steps in an epoch, and reset at the end of each epoch.


\section{Scope}

We limited the hyper-parameter search to the number of layers, kernels, regularization,
and dropout. Rectified Adam (RAdam) was used, mainly to make sure the result of resnet50
would be the same as in the notebook on Kaggle\cite{resnet:kaggle} it was based off on.

The main difference between the Kaggle notebook and the modified code used for this report is
that the Kaggle notebook uses a larger batch sizes and its use of Stratified K Fold, where K was 5.

In the Conformer model, we did not explore relative positional encoding.

We only use true positive samples, and no additional augmentation than noise, time shifting, brightness,
and masking. Since there's already a designated test set for the competition, we split the training data
into training and validation.

\newpage

\section{Methods}

\subsection{Exploratory phase}

The method used began with an exploratory phase (not reported) of a few days where several learning rates, optimizers,
and loss functions were experimented with. Once a grasp of what worked and what didn't was gained, the hyper-parameter
search started.

\subsection{Dataset shape and augmentation}

All NNs take in mel-spectrograms as input.The size of the inputs are 512 timesteps by 256 frequencies (512, 256) representing
a 1/6 of the audio file (10 seconds). A prediction is then made in the shape of a size 24 vector of logits representing the
24 classes of frog and bird audio. We take 20\% of the training data as validation data.

\subsection{Loss function, metric, optimizer}

For all training, we use sigmoid focal cross entropy with SUM\_OVER\_BATCH\_SIZE for the loss function, and lwlrap as the accuracy metric.
In the hyper-parameter search, we keep the defaults in sigmoid focal cross entropy. RAdam parameters where set to lr=1e-3, warmup\_proportion=0.3,
and min\_lr=1e-5

\subsection{Hyper-parameter search}

All networks apart from resnet50 were hyper-parameter searched using 20 trials, 16 epochs, with a batch size of 8.
Early stopping was applied using the validation lwlrap (val\_lwlrap) metric, with a patience of 4 and min delta of 0.0005.

\subsection{Model overview}

We compare three naive nets, and two already established network architectures. The naive networks were named conv, attention,
conv\_attention. Attention here denotes MHA of the performer\cite{performer} variant, due to the $O(n^2)$ cost of memory and
computation of the regular one.

The naive CNN networks were made to span the whole frequency range so as to incorporate data from all frequencies.
We also searched the receptive field in the time direction.

\subsection{Model comparison}

We take the best of each model type found through hyper-parameter searching and train them over 128 epochs of 64 batches,
each containing 16 samples. Early stopping patience is raised to 20. We then plot and compare loss and lwlrap.

\newpage

\section{Models}

The following subsections describe the types of models tested. All models had the same optimizer and optimizer settings.

\subsection{conv}

The conv model type is built up by repeating the below method 4 times.

\begin{minted}[linenos]{python}
  def conv(x, filters, kernel_h, kernel_w):
      x = Conv2D(filters=filters, kernel_size=(kernel_h, kernel_w), 
                 strides=(1, 2), padding='same', activation=relu)(x)
      skip = x
      x = Conv2D(filters=filters, kernel_size=(kernel_h, kernel_w), 
                 padding='same', activation=relu)(x)
      x = Conv2D(filters=filters, kernel_size=(kernel_h, kernel_w), 
                 padding='same', activation=relu)(x)
      x = x + skip
      x = Conv2D(filters=filters, kernel_size=(kernel_h, kernel_w), 
                 strides=(1, 2), padding='same', activation=relu)(x)
      return x
\end{minted}

After the four conv blocks, the input shape (512,256,1) is now (512,1,128).
The four conv blocks were split into two and two, each pair with their own
height and width for kernel, and number of filters as below.

\begin{minted}[linenos]{python}
  filters = hp.Int("filters_0", 16, 64)
  kernel_h = hp.Int(f"kernel_h_0", 1, 5, 2)
  kernel_w = hp.Int(f"kernel_w_0", 1, 5, 2)
  x = conv(x, filters, kernel_h, kernel_w)
  x = conv(x, filters, kernel_h, kernel_w)
  filters = hp.Int("filters_1", 64, 128)
  kernel_h = hp.Int(f"kernel_h_1", 1, 5, 2)
  kernel_w = hp.Int(f"kernel_w_1", 1, 5, 2)
  x = conv(x, filters, kernel_h, kernel_w)
  x = conv(x, filters, kernel_h, kernel_w)
\end{minted}

Lastly, we used a global max pooling layer over the time dimension
to get 128 features and a last dense layer to get the 24 logits.

\newpage

\subsection{attention}

For attention, we searched over normal encoder block as used in the Transformer model,
and Macaron\cite{macaron} type encoder block.

\begin{minted}[linenos]{python}
  def build_model(hp: kerastuner.HyperParameters)
    # [...]
    x = Lambda(lambda x: x + positional_encoding(*x.shape[1:]))(x)

    attention_block = hp.Choice("attention_block", ["normal_att", "macaron_att"])
    att_layers = hp.Int("att_layers", 1, 10)
    att_dropout = hp.Float("att_dropout", 0.0, 0.5, 0.1)
    for i in range(att_layers):
        if attention_block == "normal_att":
            x = normal_attention_block(x, att_dropout)
        else:
            x = macaron_attention_block(x, att_dropout)
    # [...]
  def normal_attention_block(x, performer_dropout):
      skip = x
      x = SelfAttention(x.shape[-1], 4, performer_dropout)(x, Variable(0.))
      x = add_and_normalize(x, skip)

      skip = x
      x = Dense(x.shape[-1], activation=relu)(x)
      x = add_and_normalize(x, skip)
      return x
  def macaron_attention_block(x, performer_dropout):
      x = pointwise_half_step_residual(x)

      skip = x
      x = SelfAttention(x.shape[-1], 4, performer_dropout)(x, Variable(0.))
      x = add_and_normalize(x, skip)

      x = pointwise_half_step_residual(x)
      return x
  def add_and_normalize(x, skip):
      x = Add()([x, skip])
      x = LayerNormalization()(x)
      return x
  def pointwise_half_step_residual(x):
      skip = x
      x = Dense(x.shape[-1])(x)
      x = ReLU()(x)
      x = Dense(x.shape[-1])(x)
      x = x * 0.5
      x = add_and_normalize(x, skip)
      return x
\end{minted}

\subsection{conv\_attention}

This model type simply takes the two described above (conv, attention) and puts the conv above
the attention so that self-attention is applied to the feature vectors across the time dimension (512, 128)
that the convolutional part output.

\subsection{resnet50}

Resnet50 is the pretrained version available in Tensorflow's model zoo. Its output goes directly
to a dense layer with 24 units like the three models above.

\subsection{conformer}

The Conformer model was implemented as described in the paper, with the code
attached as guidelines. The only difference is that we use absolute positional encoding instead of
relative positional encoding, and that we used performer MHA.

\begin{minted}[linenos]{python}
  def build_model(hp: kt.HyperParameters):
      # [...]
      dropout = hp.Float("dropout", 0.0, 0.2, 0.01)
      reg = regularizers.l2(
          hp.Float("reg", 1e-12, 1e-6, sampling="log"))
      d_model = hp.Int("d_model", 128, 256, step=32, default=256)

      x = inputs
      x = Lambda(lambda x: tf.transpose(x, (0, 2, 1, 3)))(x)
      x = Lambda(lambda x: x[:, :, :, 0:1])(x)

      x = pre_conformer_block(x, d_model, dropout, reg)

      positional_enc = positional_encoding(*x.shape[1:])
      x = x + positional_enc

      conformer_layers = hp.Int("conformer_layers", 1, 16)
      for i in range(conformer_layers):
          x = conformer_block(x, dropout, reg)
      # [...]
\end{minted}

\newpage

\section{Results}

\newcommand{\hplot}[1]{
  \par
  \begin{centering}
    \large{#1}
    \includegraphics[width=\textwidth]{plots/search_#1.png}           \\
  \end{centering}
}

\subsection{Hyper-parameter search}
\hplot{conv}
\hplot{attention}
\begin{centering}
  \large{conv\_attention}
  \includegraphics[width=\textwidth]{plots/search_conv_attention.png}           \\
\end{centering}
\hplot{conformer}

\newpage
\subsection{Best hyper-parameters}
\definecolor{black}{rgb}{0.4,0.4,0.4}
\begin{center}
  \begin{tabular}{|c|c|}
    \hline
    conv                          &                       \\
    \hline
    filters\_0                    & 64                    \\
    \hline
    kernel\_h\_0                  & 5                     \\
    \hline
    kernel\_w\_0                  & 5                     \\
    \hline
    filters\_1                    & 128                   \\
    \hline
    kernel\_h\_1                  & 5                     \\
    \hline
    kernel\_w\_1                  & 5                     \\
    \hline
    number\_of\_model\_parameters & 3795032               \\
    \hline
    \cellcolor{black}             & \cellcolor{black}     \\
    attention                     &                       \\
    \hline
    attention\_block              & macaron\_att          \\
    \hline
    att\_layers                   & 9                     \\
    \hline
    att\_dropout                  & 0.4                   \\
    \hline
    number\_of\_model\_parameters & 4747800               \\
    \hline
    \cellcolor{black}             & \cellcolor{black}     \\
    conv\_attention               &                       \\
    \hline
    filters\_0                    & 64                    \\
    \hline
    kernel\_h\_0                  & 5                     \\
    \hline
    kernel\_w\_0                  & 5                     \\
    \hline
    filters\_1                    & 128                   \\
    \hline
    kernel\_h\_1                  & 5                     \\
    \hline
    kernel\_w\_1                  & 3                     \\
    \hline
    att\_layers                   & 9                     \\
    \hline
    att\_dropout                  & 0.2                   \\
    \hline
    number\_of\_model\_parameters & 3757400               \\
    \hline
    \cellcolor{black}             & \cellcolor{black}     \\
    resnet                        &                       \\
    \hline
    number\_of\_model\_parameters & 24659352              \\
    \hline
    \cellcolor{black}             & \cellcolor{black}     \\
    conformer                     &                       \\
    \hline
    dropout                       & 0.03                  \\
    \hline
    reg                           & 4.913841429806735e-08 \\
    \hline
    d\_model                      & 192                   \\
    \hline
    conformer\_layers             & 1                     \\
    \hline
    number\_of\_model\_parameters & 5584728               \\
    \hline
  \end{tabular}
\end{center}

\newpage
\newcommand{\plot}[1]{
  \subsubsection{#1}\par
  \begin{centering}
    \begin{table}[ht]
      \makebox[\textwidth]{
        \begin{tabular}{c c}
          \includegraphics[width=0.6\textwidth]{plots/#1_train_loss.png}   & \includegraphics[width=0.6\textwidth]{plots/#1_validation_loss.png}   \\
          \includegraphics[width=0.6\textwidth]{plots/#1_train_lwlrap.png} & \includegraphics[width=0.6\textwidth]{plots/#1_validation_lwlrap.png}
        \end{tabular}
      }
    \end{table}
  \end{centering}
}

\subsection{Best model training}
\plot{conv}

\newpage
\plot{attention}

\newpage
\subsubsection{conv\_attention}\par
\begin{centering}
  \begin{table}[ht]
    \makebox[\textwidth]{
      \begin{tabular}{c c}
        \includegraphics[width=0.6\textwidth]{plots/conv_attention_train_loss.png}   & \includegraphics[width=0.6\textwidth]{plots/conv_attention_validation_loss.png}   \\
        \includegraphics[width=0.6\textwidth]{plots/conv_attention_train_lwlrap.png} & \includegraphics[width=0.6\textwidth]{plots/conv_attention_validation_lwlrap.png}
      \end{tabular}
    }
  \end{table}
\end{centering}

\newpage
\plot{resnet}

\newpage
\plot{conformer}

\newpage
\section{Discussion}

\subsection{Hyper-parameters search phase}
\subsubsection{conv}
Beginning with the hyper-parameter search, we observe that in the naive CNN network,
the search favored high kernel sizes in the time direction (width). The difference
between the height and width of the kernel sizes is potentially due to incorporate
of the whole frequency range, meaning that the network does not need wider kernels
to increase its receptive field.

Meanwhile, there is no compression of the time
dimension, and the need for larger kernels to increase the receptive field is needed.
A search over dilation instead of kernel size might be a viable option, since CNNs
tends to overfit on audio data when they have large receptive fields\cite{receptive-field}.

\subsubsection{attention}
As for attention, the search produced networks with a higher amount of layers, around 5 to 10.
It seems the attention dropout has no to little effect of its val\_lwrap performance. We also
observe that the macaron type of attention block is highly recommended above normal MHA encoder blocks.

\subsubsection{conv\_attention}
The convolutional attention stacking of the two networks discussed above do not display
a whole lot of patterns besides the already mentioned. There is a slight indication that
attention dropout now is relevant, with the best models being produced by 0.2 and 0.3.
Although this might be an interpretation based on the direct connections with val\_lwlrap
on the graph.

\subsubsection{conformer}
When it comes to the Conformer model, the d\_model (number of units in the feature vectors)
shows best results when it is at 192. It also seems like it favors 1 layer, which might be
caused by overparameterization or the need for a longer training period. Dropout ranged from
0 to 0.08, and regularization was between 1e-6 to 5e-8 for the best models.

\newpage
\subsection{Best model comparison}

All but the naive Attention network overfit the training data, and the attention network
could not converge to the level of accuracy of the other four. We observe that there is
not a large difference between the conv, conv\_attention, and conformer model. in terms
of performance on validation.

However, there is a large difference seen in the convergence of the Conformer model on training
data compared to all other models, where the Conformer gets to 0.9 lwlrap in 30 epochs while
it takes 50 epochs for resnet, 100 epochs for conv\_attention and 50 epochs for conv.

Therefore, it does not seem to be beneficial to adding attention after convolution, but rather
interweaving them with one another such as in the Conformer model. Still, the best performing
of them all was resnet, which went above 0.82, which is similar to the notebook on Kaggle.

It is also notable that resnet was fairly unstable in the first 45 epochs in the loss function,
and presumably caused a dip just after 40 epochs on the val\_lwlrap.

There is a case to be made that the dataset size is the main thing holding back
val\_lwlrap performance in this case, since all but one model overfit.

The validation loss appears to be going up even as validation lwlrap goes up. This may point to
a disconnect between the loss function's goal and the actual metric, even if they align most of the time.
A custom loss function could perhaps be brought forth to maximize the lwlrap specifically, depending on the
application. Although that might lead to Goodhart's law.

Further improvements could be gained by using mixup\cite{mixup}. One might also want to look into reducing
the receptive field\cite{receptive-field}, even if the hyper-parameter favored larger kernel sizes.

Replacing resnet with efficientnet\cite{efficientnet} may also improve performance, or at least reduce
parameter count.

A paper also studied the effects of using Performers in Conformers\cite{performers-in-conformers},
where they also saw a performance increase by using relative positional encoding, much like the
original paper on that proposed the Conformer model.

\section{Conclusion}
This report conducted a hyper-parameter search to find the best versions of four different network types
and compared them to resnet50. While all tested neural networks did worse on validation lwlrap, the Conformer
(or "performers in Conformer", PIC, as the paper that studied the effects called it) was able to converge
on the test data faster than all other neural networks. Future work would be to add more augmentation and
perhaps search the effective receptive field of the conformer model.

\newpage
\bibliographystyle{plain}
\bibliography{citations}

\end{document}
