\begin{thebibliography}{10}

\bibitem{dataset}
Rainforest connection species audio detection dataset, 2020.
\newblock https://www.kaggle.com/c/rfcx-species-audio-detection/data.

\bibitem{performer}
Krzysztof Choromanski, Valerii Likhosherstov, David Dohan, Xingyou Song,
  Andreea Gane, Tamas Sarlos, Peter Hawkins, Jared Davis, Afroz Mohiuddin,
  Lukasz Kaiser, David Belanger, Lucy Colwell, and Adrian Weller.
\newblock Rethinking attention with performers, 2020.

\bibitem{GRU}
Junyoung Chung, Caglar Gulcehre, KyungHyun Cho, and Yoshua Bengio.
\newblock Empirical evaluation of gated recurrent neural networks on sequence
  modeling, 2014.

\bibitem{LSTM}
F.~A. {Gers}, J.~{Schmidhuber}, and F.~{Cummins}.
\newblock Learning to forget: continual prediction with lstm.
\newblock In {\em 1999 Ninth International Conference on Artificial Neural
  Networks ICANN 99. (Conf. Publ. No. 470)}, volume~2, pages 850--855 vol.2,
  1999.

\bibitem{conformer}
Anmol Gulati, James Qin, Chung-Cheng Chiu, Niki Parmar, Yu~Zhang, Jiahui Yu,
  Wei Han, Shibo Wang, Zhengdong Zhang, Yonghui Wu, and Ruoming Pang.
\newblock Conformer: Convolution-augmented transformer for speech recognition,
  2020.

\bibitem{resnet}
Kaiming He, Xiangyu Zhang, Shaoqing Ren, and Jian Sun.
\newblock Deep residual learning for image recognition, 2015.

\bibitem{CNN:audio}
Shawn Hershey, Sourish Chaudhuri, Daniel P.~W. Ellis, Jort~F. Gemmeke, Aren
  Jansen, Channing Moore, Manoj Plakal, Devin Platt, Rif~A. Saurous, Bryan
  Seybold, Malcolm Slaney, Ron Weiss, and Kevin Wilson.
\newblock Cnn architectures for large-scale audio classification.
\newblock In {\em International Conference on Acoustics, Speech and Signal
  Processing (ICASSP)}. 2017.

\bibitem{receptive-field}
Khaled Koutini, Hamid Eghbal-zadeh, Matthias Dorfer, and Gerhard Widmer.
\newblock The receptive field as a regularizer in deep convolutional neural
  networks for acoustic scene classification, 2019.

\bibitem{CNN}
Y.~LeCun, B.~Boser, J.~S. Denker, D.~Henderson, R.~E. Howard, W.~Hubbard, and
  L.~D. Jackel.
\newblock Backpropagation applied to handwritten zip code recognition.
\newblock {\em Neural Computation}, 1:541--551, 1989.

\bibitem{LSTM:audio}
I.~{Lezhenin}, N.~{Bogach}, and E.~{Pyshkin}.
\newblock Urban sound classification using long short-term memory neural
  network.
\newblock In {\em 2019 Federated Conference on Computer Science and Information
  Systems (FedCSIS)}, pages 57--60, 2019.

\bibitem{macaron}
Yiping Lu, Zhuohan Li, Di~He, Zhiqing Sun, Bin Dong, Tao Qin, Liwei Wang, and
  Tie-Yan Liu.
\newblock Understanding and improving transformer from a multi-particle dynamic
  system point of view, 2019.

\bibitem{GRU:audio}
Huy Phan, Philipp Koch, Fabrice Katzberg, Marco Maass, Radoslaw Mazur, and
  Alfred Mertins.
\newblock Audio scene classification with deep recurrent neural networks, 2017.

\bibitem{efficientnet}
Mingxing Tan and Quoc~V. Le.
\newblock Efficientnet: Rethinking model scaling for convolutional neural
  networks, 2020.

\bibitem{performers-in-conformers}
Peidong Wang and DeLiang Wang.
\newblock Efficient end-to-end speech recognition using performers in
  conformers, 2020.

\bibitem{resnet:kaggle}
Yosshi999.
\newblock Rfcx: train resnet50 with tpu, 2021.
\newblock https://www.kaggle.com/yosshi999/rfcx-train-resnet50-with-tpu.

\bibitem{mixup}
Hongyi Zhang, Moustapha Cisse, Yann~N. Dauphin, and David Lopez-Paz.
\newblock mixup: Beyond empirical risk minimization, 2018.

\end{thebibliography}
