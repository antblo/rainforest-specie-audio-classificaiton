import matplotlib.patches as patches
import librosa.display
import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np
from pathlib import Path
import io
import matplotlib.pyplot as plt
import soundfile as sf
import librosa
from tqdm import tqdm
import pandas as pd
from sklearn.model_selection import StratifiedKFold
import seaborn as sns
from IPython.display import Audio
import csv

cut_time = 10
sample_time = 6
sample_rate = 48000
spec_fmax = 24000.0
spec_fmin = 40.0
spec_mel = 256
mel_power = 2
batchsize = 16
iteration_per_epoch = 64
epochs = 16
arch = tf.keras.applications.ResNet50
arch_preprocess = tf.keras.applications.resnet50.preprocess_input
freeze_to = 0

AUTOTUNE = tf.data.experimental.AUTOTUNE
data_path = "rfcx-species-audio-detection"

TRAIN_TFREC = data_path + "/tfrecords/train"
TEST_TFREC = data_path + "/tfrecords/test"


HEIGHT = spec_mel
WIDTH = 512

num_classes = 24

feature_description = {
    'recording_id': tf.io.FixedLenFeature([], tf.string, default_value=''),
    'audio_wav': tf.io.FixedLenFeature([], tf.string, default_value=''),
    'label_info': tf.io.FixedLenFeature([], tf.string, default_value=''),
}
parse_dtype = {
    'audio_wav': tf.float32,
    'recording_id': tf.string,
    'species_id': tf.int32,
    'songtype_id': tf.int32,
    't_min': tf.float32,
    'f_min': tf.float32,
    't_max': tf.float32,
    'f_max': tf.float32,
    'is_tp': tf.int32
}
