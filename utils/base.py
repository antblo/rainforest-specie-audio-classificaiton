# based on: https://www.kaggle.com/yosshi999/rfcx-train-resnet50-with-tpu
import matplotlib.patches as patches
import librosa.display
import tensorflow as tf
import tensorflow_addons as tfa
import numpy as np
from pathlib import Path
import io
import matplotlib.pyplot as plt
import soundfile as sf
import librosa
from tqdm import tqdm
import pandas as pd
from sklearn.model_selection import StratifiedKFold
import seaborn as sns
from IPython.display import Audio
import csv

from utils.get_precompute_strata import get_precompute_strata
from utils.constants import *


@tf.function
def _parse_function(example_proto):
    sample = tf.io.parse_single_example(example_proto, feature_description)
    wav, _ = tf.audio.decode_wav(
        sample['audio_wav'], desired_channels=1)
    label_info = tf.strings.split(sample['label_info'], sep='"')[1]
    labels = tf.strings.split(label_info, sep=';')

    @tf.function
    def _cut_audio(label):
        items = tf.strings.split(label, sep=',')
        spid = tf.squeeze(tf.strings.to_number(items[0], tf.int32))
        soid = tf.squeeze(tf.strings.to_number(items[1], tf.int32))
        tmin = tf.squeeze(tf.strings.to_number(items[2]))
        fmin = tf.squeeze(tf.strings.to_number(items[3]))
        tmax = tf.squeeze(tf.strings.to_number(items[4]))
        fmax = tf.squeeze(tf.strings.to_number(items[5]))
        tp = tf.squeeze(tf.strings.to_number(items[6], tf.int32))

        tmax_s = tmax * tf.cast(sample_rate, tf.float32)
        tmin_s = tmin * tf.cast(sample_rate, tf.float32)
        cut_s = tf.cast(cut_time * sample_rate, tf.float32)
        all_s = tf.cast(60 * sample_rate, tf.float32)
        tsize_s = tmax_s - tmin_s
        cut_min = tf.cast(
            tf.maximum(0.0,
                       tf.minimum(tmin_s - (cut_s - tsize_s) / 2,
                                  tf.minimum(tmax_s + (cut_s - tsize_s) / 2, all_s) - cut_s)
                       ), tf.int32
        )
        cut_max = cut_min + cut_time * sample_rate

        _sample = {
            'audio_wav': tf.reshape(wav[cut_min:cut_max], [cut_time*sample_rate]),
            'recording_id': sample['recording_id'],
            'species_id': spid,
            'songtype_id': soid,
            't_min': tmin - tf.cast(cut_min, tf.float32)/tf.cast(sample_rate, tf.float32),
            'f_min': fmin,
            't_max': tmax - tf.cast(cut_min, tf.float32)/tf.cast(sample_rate, tf.float32),
            'f_max': fmax,
            'is_tp': tp
        }
        return _sample

    samples = tf.map_fn(_cut_audio, labels, fn_output_signature=parse_dtype)
    return samples


@tf.function
def _cut_wav(x):
    # random cut in training
    cut_min = tf.random.uniform([], maxval=(
        cut_time-sample_time)*sample_rate, dtype=tf.int32)
    cut_max = cut_min + sample_time * sample_rate
    cutwave = tf.reshape(x['audio_wav'][cut_min:cut_max], [
                         sample_time*sample_rate])
    y = {}
    y.update(x)
    y['audio_wav'] = cutwave
    y['t_min'] = tf.maximum(
        0.0, x['t_min'] - tf.cast(cut_min, tf.float32) / sample_rate)
    y['t_max'] = tf.maximum(
        0.0, x['t_max'] - tf.cast(cut_min, tf.float32) / sample_rate)
    return y


@tf.function
def _cut_wav_val(x):
    # center crop in validation
    cut_min = (cut_time-sample_time)*sample_rate // 2
    cut_max = cut_min + sample_time * sample_rate
    cutwave = tf.reshape(x['audio_wav'][cut_min:cut_max], [
                         sample_time*sample_rate])
    y = {}
    y.update(x)
    y['audio_wav'] = cutwave
    y['t_min'] = tf.maximum(
        0.0, x['t_min'] - tf.cast(cut_min, tf.float32) / sample_rate)
    y['t_max'] = tf.maximum(
        0.0, x['t_max'] - tf.cast(cut_min, tf.float32) / sample_rate)
    return y


@tf.function
def _filtTP(x):
    return x['is_tp'] == 1


def show_wav(sample, ax):
    wav = sample["audio_wav"].numpy()
    rate = sample_rate
    ax.plot(np.arange(len(wav)) / rate, wav)
    ax.set_title(
        sample["recording_id"].numpy().decode()
        + ("/%d" % sample["species_id"])
        + ("TP" if sample["is_tp"] else "FP"))

    return Audio((wav * 2**15).astype(np.int16), rate=rate)


@tf.function
def _wav_to_spec(x):
    stfts = tf.signal.stft(
        x["audio_wav"], frame_length=2048, frame_step=512, fft_length=2048)
    spectrograms = tf.abs(stfts) ** mel_power

    # Warp the linear scale spectrograms into the mel-scale.
    num_spectrogram_bins = stfts.shape[-1]
    lower_edge_hertz, upper_edge_hertz, num_mel_bins = spec_fmin, spec_fmax, spec_mel

    linear_to_mel_weight_matrix = tf.signal.linear_to_mel_weight_matrix(
        num_mel_bins, num_spectrogram_bins, sample_rate, lower_edge_hertz,
        upper_edge_hertz)
    mel_spectrograms = tf.tensordot(
        spectrograms, linear_to_mel_weight_matrix, 1)
    mel_spectrograms.set_shape(spectrograms.shape[:-1].concatenate(
        linear_to_mel_weight_matrix.shape[-1:]))

    # Compute a stabilized log to get log-magnitude mel-scale spectrograms.
    log_mel_spectrograms = tf.math.log(mel_spectrograms + 1e-6)

    y = {
        # (num_mel_bins, frames)
        'audio_spec': tf.transpose(log_mel_spectrograms),
    }
    y.update(x)
    return y


def show_spectrogram(sample, ax, showlabel=False):
    S_dB = sample["audio_spec"].numpy()
    img = librosa.display.specshow(S_dB, x_axis='time',
                                   y_axis='mel', sr=sample_rate,
                                   fmax=spec_fmax, fmin=spec_fmin, ax=ax, cmap='magma')
    ax.set(
        title=f'Mel-frequency spectrogram of {sample["recording_id"].numpy().decode()}')
    sid, fmin, fmax, tmin, tmax, istp = (
        sample["species_id"], sample["f_min"], sample["f_max"], sample["t_min"], sample["t_max"], sample["is_tp"])
    ec = '#00ff00' if istp == 1 else '#0000ff'
    ax.add_patch(
        patches.Rectangle(xy=(tmin, fmin), width=tmax-tmin,
                          height=fmax-fmin, ec=ec, fill=False)
    )

    if showlabel:
        ax.text(tmin, fmax,
                f"{sid.numpy().item()} {'tp' if istp == 1 else 'fp'}",
                horizontalalignment='left', verticalalignment='bottom', color=ec, fontsize=16)


@tf.function
def _create_annot(x):
    targ = tf.one_hot(x["species_id"], num_classes,
                      on_value=x["is_tp"], off_value=0)

    return {
        'input': x["audio_spec"],
        'target': tf.cast(targ, tf.float32)
    }


@tf.function
def _preprocess_img(x, training=False):
    image = tf.expand_dims(x, axis=-1)
    image = tf.image.resize(image, [HEIGHT, WIDTH])
    image = tf.image.per_image_standardization(image)

    @tf.function
    def _specaugment(image):
        ERASE_TIME = 50
        ERASE_MEL = 16
        image = tf.expand_dims(image, axis=0)
        xoff = tf.random.uniform(
            [2], minval=ERASE_TIME//2, maxval=WIDTH-ERASE_TIME//2, dtype=tf.int32)
        xsize = tf.random.uniform(
            [2], minval=ERASE_TIME//2, maxval=ERASE_TIME, dtype=tf.int32)
        yoff = tf.random.uniform(
            [2], minval=ERASE_MEL//2, maxval=HEIGHT-ERASE_MEL//2, dtype=tf.int32)
        ysize = tf.random.uniform(
            [2], minval=ERASE_MEL//2, maxval=ERASE_MEL, dtype=tf.int32)
        image = tfa.image.cutout(
            image, [HEIGHT, xsize[0] - xsize[0] % 2], offset=[HEIGHT//2, xoff[0]])
        image = tfa.image.cutout(
            image, [HEIGHT, xsize[1] - xsize[1] % 2], offset=[HEIGHT//2, xoff[1]])
        image = tfa.image.cutout(
            image, [ysize[0] + ysize[0] % 2, WIDTH], offset=[yoff[0], WIDTH//2])
        image = tfa.image.cutout(
            image, [ysize[1] + ysize[1] % 2, WIDTH], offset=[yoff[1], WIDTH//2])
        image = tf.squeeze(image, axis=0)
        return image

    if training:
        # gaussian
        gau = tf.keras.layers.GaussianNoise(0.3)
        image = tf.cond(tf.random.uniform([]) < 0.5, lambda: gau(
            image, training=True), lambda: image)
        # brightness
        image = tf.image.random_brightness(image, 0.2)
        # specaugment
        image = tf.cond(tf.random.uniform([]) < 0.5,
                        lambda: _specaugment(image), lambda: image)

    image = (image - tf.reduce_min(image)) / (tf.reduce_max(image) -
                                              tf.reduce_min(image)) * 255.0  # rescale to [0, 255]
    image = tf.image.grayscale_to_rgb(image)
    image = arch_preprocess(image)

    return image


@tf.function
def _preprocess(x):
    image = _preprocess_img(x['input'], True)
    return (image, x["target"])


@tf.function
def _preprocess_val(x):
    image = _preprocess_img(x['input'], False)
    return (image, x["target"])


@tf.function
def _preprocess_test(x):
    image = _preprocess_img(x['audio_spec'], False)
    return (image, x["recording_id"])


def create_idx_filter(indice):
    @tf.function
    def _filt(i, x):
        return tf.reduce_any(indice == i)
    return _filt


@tf.function
def _remove_idx(i, x):
    return x


# # Other setup


def create_train_dataset(batchsize, parsed_trainval, train_idx):
    parsed_train = (parsed_trainval
                    .filter(create_idx_filter(train_idx))
                    .map(_remove_idx))

    dataset = (parsed_train.cache()
               .shuffle(len(train_idx))
               .repeat()
               .map(_cut_wav, num_parallel_calls=AUTOTUNE)
               .map(_wav_to_spec, num_parallel_calls=AUTOTUNE)
               .map(_create_annot, num_parallel_calls=AUTOTUNE)
               .map(_preprocess, num_parallel_calls=AUTOTUNE)
               .batch(batchsize))

    dataset = dataset.prefetch(AUTOTUNE)
    return dataset


def create_val_dataset(batchsize, parsed_trainval, val_idx):
    parsed_val = (parsed_trainval
                  .filter(create_idx_filter(val_idx))
                  .map(_remove_idx))

    vdataset = (parsed_val
                .map(_cut_wav_val, num_parallel_calls=AUTOTUNE)
                .map(_wav_to_spec, num_parallel_calls=AUTOTUNE)
                .map(_create_annot, num_parallel_calls=AUTOTUNE)
                .map(_preprocess_val, num_parallel_calls=AUTOTUNE)
                .batch(8)
                .cache())
    return vdataset


@tf.function
def _one_sample_positive_class_precisions(example):
    y_true, y_pred = example

    retrieved_classes = tf.argsort(y_pred, direction='DESCENDING')
    class_rankings = tf.argsort(retrieved_classes)
    retrieved_class_true = tf.gather(y_true, retrieved_classes)
    retrieved_cumulative_hits = tf.math.cumsum(
        tf.cast(retrieved_class_true, tf.float32))

    idx = tf.where(y_true)[:, 0]
    i = tf.boolean_mask(class_rankings, y_true)
    r = tf.gather(retrieved_cumulative_hits, i)
    c = 1 + tf.cast(i, tf.float32)
    precisions = r / c

    dense = tf.scatter_nd(idx[:, None], precisions, [y_pred.shape[0]])
    return dense


def _parse_function_test(example_proto):
    sample = tf.io.parse_single_example(example_proto, feature_description)
    wav, _ = tf.audio.decode_wav(
        sample['audio_wav'], desired_channels=1)  # mono

    @tf.function
    def _cut_audio(i):
        _sample = {
            'audio_wav': tf.reshape(wav[i*sample_rate*sample_time:(i+1)*sample_rate*sample_time], [sample_rate*sample_time]),
            'recording_id': sample['recording_id']
        }
        return _sample

    return tf.map_fn(_cut_audio, tf.range(60//sample_time), fn_output_signature={
        'audio_wav': tf.float32,
        'recording_id': tf.string
    })


def inference(model):
    batchsize = 60//sample_time
    tdataset = (tf.data.TFRecordDataset(tf.io.gfile.glob(TEST_TFREC + '/*.tfrec'), num_parallel_reads=AUTOTUNE)
                .map(_parse_function_test, num_parallel_calls=AUTOTUNE).unbatch()
                .map(_wav_to_spec, num_parallel_calls=AUTOTUNE)
                .map(_preprocess_test, num_parallel_calls=AUTOTUNE)
                .batch(batchsize))

    csv_header = ["recording_id", *[f"s{i}" for i in range(24)]]
    with open('submission.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',')
        csv_writer.writerow(csv_header)
        for inp, rec_ids in tqdm(tdataset):
            pred = model.predict_on_batch(
                tf.reshape(inp, [-1, HEIGHT, WIDTH, 3]))
            prob = tf.sigmoid(pred)
            prob = tf.reduce_max(tf.reshape(
                prob, [-1, 60//sample_time, num_classes]), axis=1)

            rec_ids_stack = tf.reshape(rec_ids, [-1, 60//sample_time])
            for rec in rec_ids.numpy():
                assert len(np.unique(rec)) == 1

            rec_id = rec_ids[0].numpy().decode("UTF-8")
            row = [rec_id]
            IDs = [prob[0, i].numpy() for i in range(num_classes)]

            row += IDs
            csv_writer.writerow(row)


def plot_history(history, name):
    plt.figure(figsize=(8, 3))
    plt.subplot(1, 2, 1)
    plt.plot(history.history["loss"])
    plt.plot(history.history["val_loss"])
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.title("loss")

    plt.subplot(1, 2, 2)
    plt.plot(history.history["lwlrap"])
    plt.plot(history.history["val_lwlrap"])
    plt.legend(['Train', 'Test'], loc='upper left')
    plt.title("metric")

    plt.savefig(name)


def train_and_inference(model, splits, parsed_trainval, split_id):

    loss_fn = tfa.losses.SigmoidFocalCrossEntropy(
        from_logits=True, reduction=tf.keras.losses.Reduction.SUM)

    idx_train_tf = tf.constant(splits[split_id][0])
    idx_val_tf = tf.constant(splits[split_id][1])

    dataset = create_train_dataset(batchsize, parsed_trainval, idx_train_tf)
    vdataset = create_val_dataset(batchsize, parsed_trainval, idx_val_tf)

    optimizer = tfa.optimizers.RectifiedAdam(
        lr=1e-3, total_steps=18*64, warmup_proportion=0.3, min_lr=1e-6)

    model.compile(optimizer=optimizer, loss=loss_fn,
                  metrics=[LWLRAP(num_classes)])

    history = model.fit(dataset,
                        steps_per_epoch=iteration_per_epoch,
                        epochs=epochs,
                        validation_data=vdataset,
                        callbacks=[
                            tf.keras.callbacks.ReduceLROnPlateau(
                                'val_lwlrap', patience=10
                            ),
                            tf.keras.callbacks.EarlyStopping(
                                restore_best_weights=True,
                                monitor='val_lwlrap',
                                mode="max",
                                min_delta=0.001,
                                patience=4,
                            )
                        ])
    return model, history


def load_datasets(batchsize, split_id):
    tfrecs = sorted(tf.io.gfile.glob(TRAIN_TFREC + '/*.tfrec'))
    parsed_trainval = (tf.data.TFRecordDataset(tfrecs, num_parallel_reads=AUTOTUNE)
                       .map(_parse_function, num_parallel_calls=AUTOTUNE).unbatch()
                       .filter(_filtTP).enumerate())

    splits = get_precompute_strata(parsed_trainval)

    idx_train_tf = tf.constant(splits[split_id][0])
    idx_val_tf = tf.constant(splits[split_id][1])

    dataset = create_train_dataset(batchsize, parsed_trainval, idx_train_tf)
    vdataset = create_val_dataset(batchsize, parsed_trainval, idx_val_tf)
    return dataset, vdataset
