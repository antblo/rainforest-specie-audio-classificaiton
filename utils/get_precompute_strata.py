import os.path
import pickle
import tensorflow as tf
from utils.constants import *


def get_precompute_strata(parsed_trainval):
    strata_file = "precomputed_strata.pkl"

    if os.path.isfile(strata_file):
        return pickle.load(open(strata_file, "rb"))
    else:
        indices = []
        spid = []
        recid = []

        for i, sample in tqdm(parsed_trainval.prefetch(AUTOTUNE)):
            indices.append(i.numpy())
            spid.append(sample['species_id'].numpy())
            recid.append(sample['recording_id'].numpy().decode())

        table = pd.DataFrame(
            {'indices': indices, 'species_id': spid, 'recording_id': recid})

        skf = StratifiedKFold(n_splits=5, random_state=42, shuffle=True)
        splits = list(skf.split(table.index, table.species_id))

        pickle.dump(splits, open(strata_file, "wb"))
        return splits
