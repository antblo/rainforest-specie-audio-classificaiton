import hiplot as hip
import IPython
import json
import os


def plot_hparams(tuner):
    hiplot_data_array = []
    for subdirs, dirs, files in os.walk(tuner.project_dir):
        for file in files:
            if file.endswith("trial.json"):
                with open(subdirs + '/' + file, 'r') as json_file:
                    data = json_file.read()
                    data = json.loads(data)
                    hiplot_data = data['hyperparameters']['values']
                    hiplot_data['val_lwlrap'] = data['score']
                    hiplot_data_array.append(hiplot_data)

    exp = hip.Experiment.from_iterable(hiplot_data_array)
    exp.parameters_definition["val_lwlrap"].colormap = "interpolateTurbo"
    exp.display_data(hip.Displays.TABLE).update({
        'hide': ['uid', 'from_uid'],
        'order_by': [['val_lwlrap', 'asec']]
    })
    exp.display_data(hip.Displays.PARALLEL_PLOT).update({
        'hide': ['uid', 'from_uid'],
        'order_by': [['val_lwlrap', 'asec']]
    })
    with open("hparam_html_plots/" + tuner.project_name + ".html", "w") as file:
        exp.to_html(file)
    exp.display()
