# Notes

[An End-to-End Audio Classification System based on Raw Waveforms and Mix-Training Strategy](https://arxiv.org/pdf/1911.09349.pdf)

[LABEL-EFFICIENT  AUDIO  CLASSIFICATION  THROUGH MULTI TASK LEARNING AND SELF-SUPERVISION](https://arxiv.org/pdf/1910.12587.pdf)

[Transformer tutorial](https://www.tensorflow.org/tutorials/text/transformer)

[Rejection resampling](https://www.tensorflow.org/guide/data#rejection_resampling)

[mixup](https://arxiv.org/pdf/1710.09412.pdf)

[performers](https://arxiv.org/abs/2009.14794)

[Tune library](https://docs.ray.io/en/master/tune/)

[SigmoidFocalCrossEntropy](https://arxiv.org/pdf/1708.02002.pdf)

[EfficientNet](https://arxiv.org/pdf/1905.11946.pdf)

[Conformer](https://arxiv.org/pdf/2005.08100.pdf)

[conformer code](https://github.com/TensorSpeech/TensorFlowASR/tree/main/examples/conformer)

[PERFORMERS IN CONFORMERS](https://arxiv.org/pdf/2011.04196v1.pdf)

[The Receptive Field as a regularizer](https://arxiv.org/pdf/1907.01803.pdf)

[Macaron](https://arxiv.org/abs/1906.02762)
