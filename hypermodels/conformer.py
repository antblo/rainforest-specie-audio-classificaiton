from utils.constants import *
from utils.base import load_datasets, inference
from utils.get_precompute_strata import get_precompute_strata
from utils.positional_encoding import positional_encoding
from utils.lwlrap import LWLRAP
import kerastuner as kt
import tensorflow as tf

from performer.fast_attention.tensorflow.fast_attention import SelfAttention
# Conformer as implemented in: https://github.com/TensorSpeech/TensorFlowASR/blob/main/tensorflow_asr/models/conformer.py

# https://github.com/TensorSpeech/TensorFlowASR/blob/main/tensorflow_asr/models/activations.py#L18


class GLU(tf.keras.layers.Layer):
    def __init__(self,
                 axis=-1,
                 name=None,
                 **kwargs):
        super(GLU, self).__init__(name=name, **kwargs)
        self.axis = axis

    def call(self, inputs, **kwargs):
        a, b = tf.split(inputs, 2, axis=self.axis)
        b = tf.nn.sigmoid(b)
        return tf.multiply(a, b)

    def get_config(self):
        conf = super(GLU, self).get_config()
        conf.update({"axis": self.axis})
        return conf


def pre_conformer_block(x, d_model, dropout, reg):
    filters = d_model
    units = d_model
    kernel_size = 3
    strides = 2
    x = tf.keras.layers.Conv2D(
        filters=filters, kernel_size=kernel_size,
        strides=strides, padding="same",
        kernel_regularizer=reg,
        bias_regularizer=reg
    )(x)
    x = tf.keras.layers.Reshape((x.shape[1], -1))(x)
    x = tf.keras.layers.Dense(
        units,
        kernel_regularizer=reg,
        bias_regularizer=reg
    )(x)
    x = tf.keras.layers.Dropout(dropout)(x)
    return x


def ffn_module(x, dropout, reg):
    input_dim = x.shape[-1]
    skip = x
    x = tf.keras.layers.LayerNormalization(
        gamma_regularizer=reg, beta_regularizer=reg)(x)
    x = tf.keras.layers.Dense(
        4 * input_dim, activation=tf.nn.swish,
        kernel_regularizer=reg,
        bias_regularizer=reg
    )(x)
    x = tf.keras.layers.Dropout(dropout)(x)
    x = tf.keras.layers.Dense(
        input_dim,
        kernel_regularizer=reg,
        bias_regularizer=reg
    )(x)
    x = tf.keras.layers.Dropout(dropout)(x)
    x = x * 0.5
    x = x + skip
    return x


def conv_module(x, dropout, reg):
    skip = x
    input_dim = x.shape[-1]
    depth_multiplier = 1
    x = tf.keras.layers.LayerNormalization()(x)
    x = tf.keras.layers.Reshape((x.shape[1], 1, -1))(x)
    x = tf.keras.layers.Conv2D(
        filters=2 * input_dim, kernel_size=1, strides=1,
        padding="valid",
        kernel_regularizer=reg,
        bias_regularizer=reg
    )(x)
    x = GLU()(x)
    x = tf.keras.layers.DepthwiseConv2D(
        kernel_size=(32, 1), strides=1,
        padding="same",
        depth_multiplier=depth_multiplier,
        depthwise_regularizer=reg,
        bias_regularizer=reg
    )(x)
    x = tf.keras.layers.BatchNormalization(
        gamma_regularizer=reg, beta_regularizer=reg)(x)
    x = tf.keras.layers.Activation(tf.nn.swish)(x)
    x = tf.keras.layers.Conv2D(
        filters=input_dim, kernel_size=1, strides=1,
        padding="valid",
        kernel_regularizer=reg,
        bias_regularizer=reg
    )(x)
    x = tf.keras.layers.Dropout(dropout)(x)
    x = tf.keras.layers.Reshape((x.shape[1], -1))(x)
    x = x + skip
    return x


def multi_head_attention_module(x, dropout, reg):
    skip = x
    x = tf.keras.layers.LayerNormalization(
        gamma_regularizer=reg, beta_regularizer=reg)(x)
    x = SelfAttention(x.shape[-1], 4, 0.1)(x, tf.Variable(0.))
    x = tf.keras.layers.Dropout(dropout)(x)
    x = x + skip
    return x


def conformer_block(x, dropout, reg):
    x = ffn_module(x, dropout, reg)
    x = multi_head_attention_module(x, dropout, reg)
    x = conv_module(x, dropout, reg)
    x = ffn_module(x, dropout, reg)
    x = tf.keras.layers.LayerNormalization(
        gamma_regularizer=reg,
        beta_regularizer=reg
    )(x)
    return x


def build_model(hp: kt.HyperParameters):  # https://arxiv.org/pdf/2009.09632v1.pdf
    input_shape = (HEIGHT, WIDTH, 3)
    inputs = tf.keras.Input(shape=input_shape)

    dropout = hp.Float("dropout", 0.0, 0.2, 0.01)
    reg = tf.keras.regularizers.l2(
        hp.Float("reg", 1e-12, 1e-6, sampling="log"))
    d_model = hp.Int("d_model", 128, 256, step=32, default=256)

    x = inputs
    x = tf.keras.layers.Lambda(lambda x: tf.transpose(x, (0, 2, 1, 3)))(x)
    x = tf.keras.layers.Lambda(lambda x: x[:, :, :, 0:1])(x)

    x = pre_conformer_block(x, d_model, dropout, reg)

    positional_enc = positional_encoding(*x.shape[1:])
    x = x + positional_enc

    conformer_layers = hp.Int("conformer_layers", 1, 16)
    for i in range(conformer_layers):
        x = conformer_block(x, dropout, reg)

    x = tf.keras.layers.GlobalMaxPool1D()(x)
    # negative bias for stability (Section 4.1 in "Focal Loss" (https://arxiv.org/abs/1708.02002))
    prior = 1/num_classes
    pi = -np.log((1-prior) / prior)  # sigmoid(pi) == prior
    x = tf.keras.layers.Dense(
        num_classes, bias_initializer=tf.keras.initializers.Constant(pi))(x)
    model = tf.keras.Model(inputs, x, name="conformer")

    loss_fn = tfa.losses.SigmoidFocalCrossEntropy(
        from_logits=True, reduction=tf.keras.losses.Reduction.SUM_OVER_BATCH_SIZE)
    optimizer = tfa.optimizers.RectifiedAdam(
        lr=1e-3, total_steps=epochs*iteration_per_epoch, warmup_proportion=0.3, min_lr=1e-5)
    model.compile(optimizer=optimizer, loss=loss_fn,
                  metrics=[LWLRAP(num_classes)])
    return model
