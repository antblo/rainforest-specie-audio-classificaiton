from utils.constants import *
from utils.base import load_datasets, inference
from utils.get_precompute_strata import get_precompute_strata
from utils.positional_encoding import positional_encoding
from utils.lwlrap import LWLRAP
import kerastuner as kt
import tensorflow as tf


def conv(x, filters, kernel_h, kernel_w):
    x = tf.keras.layers.Conv2D(filters=filters, kernel_size=(
        kernel_h, kernel_w), strides=(1, 2), padding='same', activation=tf.nn.relu)(x)
    skip = x
    x = tf.keras.layers.Conv2D(filters=filters, kernel_size=(
        kernel_h, kernel_w), padding='same', activation=tf.nn.relu)(x)
    x = tf.keras.layers.Conv2D(filters=filters, kernel_size=(
        kernel_h, kernel_w), padding='same', activation=tf.nn.relu)(x)
    x = x + skip
    x = tf.keras.layers.Conv2D(filters=filters, kernel_size=(
        kernel_h, kernel_w), strides=(1, 2), padding='same', activation=tf.nn.relu)(x)
    return x


def conv_block(x, hp):  # https://arxiv.org/pdf/1907.01803.pdf
    filters = hp.Int("filters_0", 16, 64)
    kernel_h = hp.Int(f"kernel_h_0", 1, 5, 2)
    kernel_w = hp.Int(f"kernel_w_0", 1, 5, 2)
    x = conv(x, filters, kernel_h, kernel_w)
    x = conv(x, filters, kernel_h, kernel_w)
    filters = hp.Int("filters_1", 64, 128)
    kernel_h = hp.Int(f"kernel_h_1", 1, 5, 2)
    kernel_w = hp.Int(f"kernel_w_1", 1, 5, 2)
    x = conv(x, filters, kernel_h, kernel_w)
    x = conv(x, filters, kernel_h, kernel_w)
    x = tf.keras.layers.Reshape((*x.shape[1:2], -1))(x)
    return x


def build_model(hp: kt.HyperParameters):
    input_shape = (HEIGHT, WIDTH, 3)
    inputs = tf.keras.Input(shape=input_shape)
    x = inputs
    x = tf.keras.layers.Lambda(lambda x: tf.transpose(x, (0, 2, 1, 3)))(x)

    x = tf.keras.layers.Lambda(lambda x: x[:, :, :, 0:1])(x)
    x = conv_block(x, hp)

    x = tf.keras.layers.GlobalMaxPool1D()(x)
    # negative bias for stability (Section 4.1 in "Focal Loss" (https://arxiv.org/abs/1708.02002))
    prior = 1/num_classes
    pi = -np.log((1-prior) / prior)  # sigmoid(pi) == prior
    x = tf.keras.layers.Dense(
        num_classes, bias_initializer=tf.keras.initializers.Constant(pi))(x)
    model = tf.keras.Model(inputs, x, name="conv")

    loss_fn = tfa.losses.SigmoidFocalCrossEntropy(
        from_logits=True, reduction=tf.keras.losses.Reduction.SUM_OVER_BATCH_SIZE)
    optimizer = tfa.optimizers.RectifiedAdam(
        lr=1e-3, total_steps=epochs*iteration_per_epoch, warmup_proportion=0.3, min_lr=1e-5)
    model.compile(optimizer=optimizer, loss=loss_fn,
                  metrics=[LWLRAP(num_classes)])
    return model
