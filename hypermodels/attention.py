from utils.constants import *
from utils.base import load_datasets, inference
from utils.get_precompute_strata import get_precompute_strata
from utils.positional_encoding import positional_encoding
from utils.lwlrap import LWLRAP
import kerastuner as kt
import tensorflow as tf

from performer.fast_attention.tensorflow.fast_attention import SelfAttention


def add_and_normalize(x, skip):
    x = tf.keras.layers.Add()([x, skip])
    x = tf.keras.layers.LayerNormalization()(x)
    return x


def pointwise_half_step_residual(x):
    skip = x
    x = tf.keras.layers.Dense(x.shape[-1])(x)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.Dense(x.shape[-1])(x)
    x = x * 0.5
    x = add_and_normalize(x, skip)
    return x


# https://arxiv.org/pdf/1906.02762.pdf
def macaron_attention_block(x, performer_dropout):
    x = pointwise_half_step_residual(x)

    skip = x
    x = SelfAttention(x.shape[-1], 4, performer_dropout)(x, tf.Variable(0.))
    x = add_and_normalize(x, skip)

    x = pointwise_half_step_residual(x)
    return x


def normal_attention_block(x, performer_dropout):
    skip = x
    x = SelfAttention(x.shape[-1], 4, performer_dropout)(x, tf.Variable(0.))
    x = add_and_normalize(x, skip)

    skip = x
    x = tf.keras.layers.Dense(x.shape[-1], activation=tf.nn.relu)(x)
    x = add_and_normalize(x, skip)
    return x


def build_model(hp: kt.HyperParameters):
    input_shape = (HEIGHT, WIDTH, 3)
    inputs = tf.keras.Input(shape=input_shape)
    x = inputs
    x = tf.keras.layers.Lambda(lambda x: tf.transpose(x, (0, 2, 1, 3)))(x)

    x = tf.keras.layers.Lambda(lambda x: x[:, :, :, 0])(x)
    x = tf.keras.layers.Lambda(
        lambda x: x + positional_encoding(*x.shape[1:]))(x)

    attention_block = hp.Choice(
        "attention_block", ["normal_att", "macaron_att"])
    att_layers = hp.Int("att_layers", 1, 10)
    att_dropout = hp.Float("att_dropout", 0.0, 0.5, 0.1)
    for i in range(att_layers):
        if attention_block == "normal_att":
            x = normal_attention_block(x, att_dropout)
        else:
            x = macaron_attention_block(x, att_dropout)

    x = tf.keras.layers.GlobalMaxPool1D()(x)

    # negative bias for stability (Section 4.1 in "Focal Loss" (https://arxiv.org/abs/1708.02002))
    prior = 1/num_classes
    pi = -np.log((1-prior) / prior)  # sigmoid(pi) == prior
    x = tf.keras.layers.Dense(
        num_classes, bias_initializer=tf.keras.initializers.Constant(pi))(x)
    model = tf.keras.Model(inputs, x, name="attention")

    loss_fn = tfa.losses.SigmoidFocalCrossEntropy(
        from_logits=True, reduction=tf.keras.losses.Reduction.SUM_OVER_BATCH_SIZE)
    optimizer = tfa.optimizers.RectifiedAdam(
        lr=1e-3, total_steps=epochs*iteration_per_epoch, warmup_proportion=0.3, min_lr=1e-5)
    model.compile(optimizer=optimizer, loss=loss_fn,
                  metrics=[LWLRAP(num_classes)])
    return model
