from utils.constants import *
from utils.base import load_datasets, inference
from utils.get_precompute_strata import get_precompute_strata
from utils.positional_encoding import positional_encoding
from utils.lwlrap import LWLRAP
import kerastuner as kt
import tensorflow as tf

from performer.fast_attention.tensorflow.fast_attention import SelfAttention


def conv(x, filters, kernel_h, kernel_w):
    x = tf.keras.layers.Conv2D(filters=filters, kernel_size=(
        kernel_h, kernel_w), strides=(1, 2), padding='same', activation=tf.nn.relu)(x)
    skip = x
    x = tf.keras.layers.Conv2D(filters=filters, kernel_size=(
        kernel_h, kernel_w), padding='same', activation=tf.nn.relu)(x)
    x = tf.keras.layers.Conv2D(filters=filters, kernel_size=(
        kernel_h, kernel_w), padding='same', activation=tf.nn.relu)(x)
    x = x + skip
    x = tf.keras.layers.Conv2D(filters=filters, kernel_size=(
        kernel_h, kernel_w), strides=(1, 2), padding='same', activation=tf.nn.relu)(x)
    return x


def conv_block(x, hp):  # https://arxiv.org/pdf/1907.01803.pdf
    filters = hp.Int("filters_0", 16, 64, 4)
    kernel_h = hp.Int(f"kernel_h_0", 3, 5, 2)
    kernel_w = hp.Int(f"kernel_w_0", 3, 5, 2)
    x = conv(x, filters, kernel_h, kernel_w)
    x = conv(x, filters, kernel_h, kernel_w)
    filters = hp.Int("filters_1", 64, 128, 4)
    kernel_h = hp.Int(f"kernel_h_1", 3, 5, 2)
    kernel_w = hp.Int(f"kernel_w_1", 3, 5, 2)
    x = conv(x, filters, kernel_h, kernel_w)
    x = conv(x, filters, kernel_h, kernel_w)
    x = tf.keras.layers.Reshape((*x.shape[1:2], -1))(x)
    return x


def add_and_normalize(x, skip):
    x = tf.keras.layers.Add()([x, skip])
    x = tf.keras.layers.LayerNormalization()(x)
    return x


def pointwise_half_step_residual(x):
    skip = x
    x = tf.keras.layers.Dense(x.shape[-1])(x)
    x = tf.keras.layers.Activation(tfa.activations.mish)(x)
    x = tf.keras.layers.Dense(x.shape[-1])(x)
    x = x * 0.5
    x = add_and_normalize(x, skip)
    return x


def macaron_attention_block(x, performer_dropout):
    x = pointwise_half_step_residual(x)

    skip = x
    x = SelfAttention(x.shape[-1], 4, performer_dropout)(x, tf.Variable(0.))
    x = add_and_normalize(x, skip)

    x = pointwise_half_step_residual(x)
    return x


def build_model(hp: kt.HyperParameters):  # https://arxiv.org/pdf/2009.09632v1.pdf
    input_shape = (HEIGHT, WIDTH, 3)
    inputs = tf.keras.Input(shape=input_shape)
    x = inputs
    x = tf.keras.layers.Lambda(lambda x: tf.transpose(x, (0, 2, 1, 3)))(x)
    x = tf.keras.layers.Lambda(lambda x: x[:, :, :, 0:1])(x)

    x = conv_block(x, hp)

    positional_enc = positional_encoding(*x.shape[1:])
    x = x + positional_enc

    att_layers = hp.Int("att_layers", 1, 9)
    att_dropout = hp.Float("att_dropout", 0.0, 0.5, 0.1)
    for i in range(att_layers):
        x = macaron_attention_block(x, att_dropout)

    x = tf.keras.layers.GlobalMaxPool1D()(x)
    # negative bias for stability (Section 4.1 in "Focal Loss" (https://arxiv.org/abs/1708.02002))
    prior = 1/num_classes
    pi = -np.log((1-prior) / prior)  # sigmoid(pi) == prior
    x = tf.keras.layers.Dense(
        num_classes, bias_initializer=tf.keras.initializers.Constant(pi))(x)
    model = tf.keras.Model(inputs, x, name="conv_attention")

    loss_fn = tfa.losses.SigmoidFocalCrossEntropy(
        from_logits=True, reduction=tf.keras.losses.Reduction.SUM_OVER_BATCH_SIZE)
    optimizer = tfa.optimizers.RectifiedAdam(
        lr=1e-3, total_steps=epochs*iteration_per_epoch, warmup_proportion=0.3, min_lr=1e-5)
    model.compile(optimizer=optimizer, loss=loss_fn,
                  metrics=[LWLRAP(num_classes)])
    return model
